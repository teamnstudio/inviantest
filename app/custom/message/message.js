let m;
let position;
exports.loaded = function (args) {
    m = args.object;
    if (m.verticalAlignment == 'top'){
        position = -150;
    }else{
        position = 150;
    }
}
exports.messageShow = function (b, i, t, s, d) {
    m.translateY = position;
    m.className = b;
    m.visibility = 'visible';
    const icon = m.getChildAt(0);
    const title = m.getChildAt(1);
    const subtitle = m.getChildAt(2);
    icon.text = i;
    title.text = t;
    subtitle.text = s;
    fadeIn(m);
    if (d) {
        setTimeout(() => {
            fadeOut(m);
        }, d)
    }
}
function fadeIn(m) {
    m.animate({ translate: { x: 0, y: 0 }, duration: 200 })
        .then(function () {

        })
        .catch(function () {

        });
}
function fadeOut(m) {
    m.animate({ translate: { x: 0, y: position }, duration: 200 })
        .then(function () {
            m.visibility = 'collapsed';
        })
        .catch(function () {

        });
}
exports.hideMessage = function (args) {
    fadeOut(args.object);
}
