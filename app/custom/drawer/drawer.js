const ls = require('~/common/ls');
const nav = require('~/common/nav');
let page;
let actionBar = false;
exports.loaded = function (args) {
    page = args.object;
    setProfile();
}
function setProfile() {
    const profile = ls.getJson('profile');
    page.getViewById('user_name').text = profile.person.name + ' ' + profile.person.lastname;
    page.getViewById('user_logo').src = ls.getString('aws') + profile.person.avatar_url;
    setServices(profile.client.id);
}
function setServices(id) {
    const services = ls.getJson('services');
    for (const key in services) {
        if (page.getViewById('service_' + key)) {
            page.getViewById('service_' + key).visibility = 'visible';
        }
    }
    if (id == 1) {
        page.getViewById('service_0').visibility = 'visible';
    }
}
exports.tapOpenDrawer = function (a) {
    if (a) {
        actionBar = a;
        actionBar.getViewById('actionBar_title').visibility = 'collapsed';
        actionBar.getViewById('actionBar_btn_left').visibility = 'collapsed';
    } else {
        page.getChildAt(0).paddingTop = 50;
    }
    fadeIn();
}
exports.tapHideDrawer = function () {
    fadeOut();
}
function fadeIn() {
    page.opacity = 0;
    page.visibility = 'visible';
    page.animate({ translate: { x: 0, y: -700 }, duration: 1 })
        .then(function () {
            page.opacity = 1;
            return page.animate({ translate: { x: 0, y: 0 }, duration: 300 })
        })
        .then(function () {
            page.className = "bg_disabled";
        })
        .catch(function () {

        });
}
function fadeOut() {
    if (actionBar) {
        actionBar.getViewById('actionBar_title').visibility = 'visible';
        actionBar.getViewById('actionBar_btn_left').visibility = 'visible';
    }
    page.className = "";
    page.animate({ translate: { x: 0, y: -700 }, duration: 300 })
        .then(function () {
            page.visibility = 'collapsed';
        })
        .catch(function () {

        });
}
//btn add ios
exports.btnLoaded = function (args) {
    const layer = args.object.ios.layer;
    // eslint-disable-next-line no-undef
    layer.borderColor = UIColor.whiteColor.CGColor; //UIColor[args.object.bg_color + 'Color'].CGColor; //(args.object.bg_color == 'white' ? UIColor.whiteColor.CGColor : UIColor.yellowColor.CGColor);
    // eslint-disable-next-line no-undef
    layer.shadowOffset = CGSizeMake(0, 2);
    layer.shadowOpacity = 0.1;
    layer.shadowRadius = 5;
    layer.cornerRadius = 15;
}
exports.tapGoTo = function (args) {
    ls.setBool('clear', true);
    ls.setString('home', args.object.route);
    nav.goTo(page.page, args.object.route, false, false, true);
}