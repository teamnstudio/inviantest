const ViewModel = require('./charts-view-model');

let page;
exports.loaded = function(args) {
        page = args.object;
        page.bindingContext = ViewModel;
    }
    /*
        The chartOptions is a HICharts options object, refer to https://www.highcharts.com/demo/ for inspiration,
        and also checkout https://api.highcharts.com/highcharts/ for API reference
    */
exports.chartViewLoaded = function(args) {
    console.log('chartViewLoaded()');

    const chartView = args.object;

    const options = {

        title: {
            text: 'Logarithmic axis demo'
        },

        xAxis: {
            tickInterval: 1,
            type: 'logarithmic',
            accessibility: {
                rangeDescription: 'Range: 1 to 10'
            }
        },

        yAxis: {
            type: 'logarithmic',
            minorTickInterval: 0.1,
            accessibility: {
                rangeDescription: 'Range: 0.1 to 1000'
            }
        },

        tooltip: {
            headerFormat: '<b>{series.name}</b><br />',
            pointFormat: 'x = {point.x}, y = {point.y}'
        },

        series: [{
            data: [1, 2, 4, 8, 16, 32, 64, 128, 256, 512],
            pointStart: 1
        }]
    }

    chartView.setOptions(options);

}