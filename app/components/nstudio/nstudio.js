const ViewModel = require('./nstudio-view-model');

const VideoEditor = require('@viia/nativescript-video-editor');

let page;
exports.loaded = function(args) {
    page = args.object;
    page.bindingContext = ViewModel;
    ViewModel.set('value', undefined);
}
exports.testIt = function(args) {
    console.log(args.object.text);
}
exports.openCamera = function(args) {
    console.log(args.object.text);
}