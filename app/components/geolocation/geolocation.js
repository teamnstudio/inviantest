const ViewModel = require('./geolocation-view-model');

const ls = require('~/common/ls');
const gps = require('~/common/gps');

let page;
exports.loaded = function(args) {
    page = args.object;
    setInit();
    page.bindingContext = ViewModel;
}

function setInit() {
    ViewModel.set('loaded', true);
    ViewModel.set('width', ls.getNumber('screenWidth'));
    ViewModel.set('height', ls.getNumber('screenHeight'));
}
exports.tapAdd = function() {
    console.log('tapAdd()');
    ViewModel.set('loaded', false);
    gps.getGeoLocation().
    then((r) => {
        ViewModel.set('loaded', true);
        console.log(r);
        setContent(r);
    }, (e) => {
        ViewModel.set('loaded', true);
        console.log(e);
        setContent({ error: e });
    });
}

function setContent(items) {
    console.log('setContent()');
    const resp = [];
    for (const item in items) {
        resp.push({
            key: item,
            value: items[item]
        });
    }
    ViewModel.set('items', resp);
}