exports.goBack = function(page) {
    page.frame.goBack();
};
exports.goTo = function(page, nav, context, transition, history, backstack) {
    goToFunc(page, nav, context, transition, history, backstack);
};

function goToFunc(page, nav, context, transition, history, backstack) {
    page.frame.navigate({
        moduleName: "./components/" + nav,
        context: context,
        animated: true,
        transition: getTransition(transition),
        clearHistory: history,
        backstackVisible: backstack
    });

    // const tappedCarItem = args.view.bindingContext
    // Frame.topmost().navigate({
    //     moduleName: 'cars/car-detail-page/car-detail-page',
    //     context: tappedCarItem,
    //     animated: true,
    //     transition: {
    //         name: 'slide',
    //         duration: 200,
    //         curve: 'ease',
    //     },
    // })
}

function getTransition(type) {
    let transition;
    switch (type) {
        case 'slide':
            transition = {
                name: "slide",
                curve: "ease"
            };
            break;
        case 'slideTop':
            transition = {
                name: "slideTop",
                curve: "ease"
            };
            break;
        case 'slideBottom':
            transition = {
                name: "slideBottom",
                curve: "ease"
            };
            break;
        default:
            transition = {
                name: "fade",
                curve: "ease"
            };
            break;
    }
    return transition;
}
exports.error = function(page, code) {
    switch (code) {
        case 401:
            goToFunc(page, 'login/login', true, false, false, false);
            break;
        default:
            break;
    }
};