var bghttp = require('@nativescript/background-http');
var session = bghttp.session('image-upload');

const ls = require('~/common/ls');
const files = require('~/common/files');

function bytesToSize(bytes) {
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
    if (bytes == 0) return 'n/a';
    let i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
    if (i == 0) return bytes + ' ' + sizes[i];
    return (bytes / Math.pow(1024, i)).toFixed(1) + ' ' + sizes[i];
}

function moveFiles(file_name) {
    console.log('===MOVE===');
    const items = ls.getJson(file_name);
    console.log(items);
    if (items) {
        let file = items.move;
        if (file) {
            const data = [];
            file.forEach(item => {
                const name = getNameFromFile(item.file_url);
                data.push({
                    fileorig: item.file_url,
                    pathdest: item.move,
                    filename: name
                })
            });
            files.copyFileMultiple(data)
                .then((r) => {
                    console.log('SE MOVIO:');
                    console.log(r);
                    removeFiles(file_name);
                }).catch((e) => {
                    console.log('ERROR AL MOVER: ' + file_name);
                    console.log(e);
                    setNextLoop(file_name);
                });
        } else {
            setNextLoop(file_name); //ojo nuevo servicio de inc
        }
    }
}

function removeFiles(file_name) {
    console.log('===REMOVE===');
    const items = ls.getJson(file_name);
    if (items) {
        const file = items.remove;
        const data = [];
        if (file) {
            file.forEach(item => {
                data.push({
                    path: item.file_url,
                    folder: '',
                    file: ''
                })
            });
            files.removeFileMultiple(data)
                .then((r) => {
                    console.log('SE REMOVIÓ:');
                    console.log(r);
                    setNextLoop(file_name);
                }).catch((e) => {
                    console.log('ERROR AL REMOVER: ' + file_name);
                    console.log(e);
                    setNextLoop(file_name);
                });
        }
    }
}

function onEvent(e) {
    // console.log({
    //     color: {
    //         eventName: e.eventName,
    //         responseCode: e.responseCode
    //     },
    //     eventTitle: e.eventName + " " + e.object.description,
    //     eventData: JSON.stringify({
    //         error: e.error ? e.error.toString() : e.error,
    //         currentBytes: e.currentBytes,
    //         totalBytes: e.totalBytes,
    //         body: e.data,
    //         responseCode: e.responseCode
    //     })
    // });
    switch (e.eventName) {
        case 'complete':
            console.log('========= BG complete ========= ', e.responseCode);
            if (e.responseCode == 200 || e.responseCode == 201) {
                if (ls.getBool('save' + getTypeFile(e.object.description))) {
                    moveFiles(e.object.description);
                } else {
                    removeFiles(e.object.description);
                }
            } else {
                // moveFiles(e.object.description); //OJO MOVE TO BACKUP ALL
                setNextLoop(e.object.description);
            }
            break;
        case 'error':
            console.log('========= BG error ============', e.responseCode);
            // moveFiles(e.object.description); //OJO MOVE TO BACKUP ALL
            setNextLoop(e.object.description);
            break;
        default:
            break;
    }
}

function getExtensionFromFile(file_name) {
    const last = file_name.lastIndexOf('.');
    return file_name.substring(last, file_name.length);
}

function getTypeFile(file_name) {
    file_name = getExtensionFromFile(file_name);
    console.log('EXTENSION:', file_name);
    switch (file_name) {
        case '.mp4':
            return 'Videos';
        case '.jpg':
            return 'Images';
        default:
            return 'Images';
    }
}

function setNextLoop(file_name) {
    const setNextLoop = ls.getNumber('bg_loop') + 1;
    console.log('setNextLoop: ', file_name);
    console.log('setNextLoop: ', setNextLoop);
    ls.setNumber('bg_loop', setNextLoop);
    deleteLsJsonContent(file_name);
}
exports.nextLoop = function(file_name) {
        setNextLoop(file_name);
    }
    // function getPathFromFile(item) {
    //     const last = item.lastIndexOf('/');
    //     return item.substring(0, last);
    // }
function getNameFromFile(item) {
    const last = item.lastIndexOf('/');
    return item.substring(last + 1, item.length);
}

function getJsonName(item) {
    const last = item.lastIndexOf('.');
    return item.substring(0, last) + '.json';
}

function setLsJsonContent(file_name, file_url, content, move, remove) {
    ls.setJson(file_name, {
        file_url: file_url,
        content: content,
        move: move,
        remove: remove
    });
}

function deleteLsJsonContent(file_name) {
    ls.remove(file_name);
}
exports.moveFiles = function(file_name, move, remove) {
    console.log('moveFiles:', file_name);
    setLsJsonContent(file_name, false, false, move, remove);
    moveFiles(file_name)
}
exports.sendBackgroundImage = function(file_url, content, move, remove) {
    console.log('sendBackgroundImage()');
    const size = files.getSizeFile(file_url);
    const file_name = getNameFromFile(file_url);
    console.log('size: ' + size);
    setLsJsonContent(file_name, file_url, content, move, remove);
    if (size > 0) {
        console.log('size: ' + bytesToSize(size));
        const url = ls.getString('lambda') + 'upload/mobile';
        console.log(url);
        const request = {
            url: url,
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data', //'application/octet-stream',
                'file-name': file_name,
                'Content': JSON.stringify(content)
            },
            androidAutoClearNotification: true,
            description: file_name
        };
        const params = [
            { name: "fileToUpload", filename: file_url, mimeType: 'image/jpg' }
        ];
        const task = session.multipartUpload(params, request);
        task.on('progress', onEvent.bind(this));
        task.on('error', onEvent.bind(this));
        task.on('responded', onEvent.bind(this));
        task.on('complete', onEvent.bind(this));
    } else {
        console.log('====== DELETE SIZE 0 ========', file_url);
        moveFiles(file_name);
    }
}
exports.sendBackgroundVideo = function(file_url, content, move, remove) {
    console.log('sendBackgroundVideo()');
    const size = files.getSizeFile(file_url);
    const file_name = getNameFromFile(file_url);
    console.log('size: ' + size);
    setLsJsonContent(file_name, file_url, content, move, remove);
    if (size > 0) {
        console.log('size: ' + bytesToSize(size));
        const request = {
            url: ls.getString('phpserver'), // + 'upload',
            method: 'POST',
            headers: {
                'x-device-type': 1,
                'Authorization': 'Bearer ' + ls.getString('token'),
                'Content-Type': 'application/octet-stream', //form-data
                'file-name': file_name,
                'Content': JSON.stringify(content)
            },
            androidAutoClearNotification: true,
            description: file_name
        };
        const task = session.uploadFile(file_url, request);

        task.on('progress', onEvent.bind(this));
        task.on('error', onEvent.bind(this));
        task.on('responded', onEvent.bind(this));
        task.on('complete', onEvent.bind(this));
    } else {
        console.log('====== DELETE SIZE 0 ========', file_url);
        moveFiles(file_name);
    }
}

function getParamsMultipart(items) {
    const content = [];
    for (const k in items) {
        content.push({
            name: k,
            value: items[k]
        })
    }
    return content;
}
exports.sendBackgroundUploads = function(file_url, content, move, remove, service) {
    console.log('sendBackgroundUploads()');
    const size = files.getSizeFile(file_url);
    const file_name = getNameFromFile(file_url);
    console.log('size: ' + size);
    setLsJsonContent(file_name, file_url, content, move, remove);
    if (size > 0) {
        console.log('size: ' + bytesToSize(size));
        const url = ls.getString('uploads_base_url') + service;
        console.log('URL: ', url);
        console.log('Bearer ' + ls.getString('token'));
        const request = {
            url: url,
            method: 'POST',
            headers: {
                'x-device-type': 1,
                'Authorization': 'Bearer ' + ls.getString('token'),
                'Content-Type': 'form-data', // 'application/octet-stream'
                'file-name': file_name,
            },
            utf8: true,
            androidAutoClearNotification: true,
            description: file_name
        };
        const params = getParamsMultipart(content);
        params.push({ name: 'file', filename: file_url, mimeType: 'image/jpg' });

        var task = session.multipartUpload(params, request);

        task.on('progress', onEvent.bind(this));
        task.on('error', onEvent.bind(this));
        task.on('responded', onEvent.bind(this));
        task.on('complete', onEvent.bind(this));
    } else {
        console.log('====== DELETE SIZE 0 ========', file_url);
        moveFiles(file_name);
    }
}

exports.writeJsonAndMoveFile = function(item) {
    console.log('writeJsonAndMoveFile()');
    console.dir(item);
    if (item.move) {
        console.log('==========writeJsonAndMoveFile============');
        const file_name = item.file_name;
        const json_name = getJsonName(item.file_name);
        const newitem = {
            file_url: item.move + '/' + file_name,
            content: item.content,
            move: [{
                move: ls.getString('path_backup_files'),
                file_url: item.move + '/' + file_name
            }, {
                move: ls.getString('path_backup_files'),
                file_url: item.move + '/' + json_name
            }],
            remove: [{
                file_url: item.move + '/' + file_name
            }, {
                file_url: item.move + '/' + json_name
            }]
        };
        console.log('newitem:');
        console.log(newitem);
        files.writeFileText(item.move, json_name, JSON.stringify(newitem));
        files.copyFile(item.file_url, item.move, file_name)
            .then(() => {
                // files.removeFile(item.file_url, '', '');
            }).catch((e) => {
                console.log('ERROR AL MOVER: ' + file_name);
                console.log(e);
            });
    } else {
        console.log('========ERROR======== writeJsonAndMoveFile no exist move')
    }
}



/*
Add line in 270 background-http.android.js
function setRequestOptions(request, options) {
    //FIX TIMEOUT => 5min
    net.gotev.uploadservice.UploadService.HTTP_STACK = new  net.gotev.uploadservice.http.impl.HurlStack(true, false, 5*60*1000, 5*60*1000);
    //
*/