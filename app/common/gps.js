const geolocation = require('@nativescript/geolocation');
const ls = require('~/common/ls');
exports.getGeoLocation = function() {
    ls.setNumber("latitude", 0);
    ls.setNumber("longitude", 0);
    geolocation.enableLocationRequest();
    const promise = geolocation.getCurrentLocation({ desiredAccuracy: 3, updateDistance: 10, maximumAge: 20000, timeout: 20000 }).
    then(function(loc) {
        if (loc) {
            ls.setNumber("latitude", loc.latitude);
            ls.setNumber("longitude", loc.longitude);
            return loc;
        }
    }, function(e) {
        console.log(e);
        return 'Error al obtener GPS'
    });
    return promise;
}