const ls = require('~/common/ls');
exports.setConfig = function(platform, sdk, isDev) {
    ls.setString('version', '0.0.0');
    ls.setNumber('pag', 20);
    ls.setNumber('sdk', sdk);
    ls.setString('platform', platform);
    ls.setString('home', 'general/general');
    ls.setString('noimg', '~/images/no_image');
    ls.setString('noavatar', '~/images/avatar');
    ls.setString('aws', 'https://s3.amazonaws.com/');
    ls.setBool('inc_isImg', true);
    ls.setNumber('bg_loop', 0);
    if (isDev) {
        ls.setString('connection', 'https://dev-api.viiamanager.com/');
        ls.setString('lambda', 'https://dev-lambda.viiamanager.com/');
        ls.setString('phpserver', 'https://dev-upload.viiamanager.com/');
        ls.setString('uploads_base_url', 'https://dev-uploads.viiamanager.com/');
    } else {
        ls.setString('connection', 'https://api.viiamanager.com/');
        ls.setString('lambda', 'https://lambda.viiamanager.com/');
        ls.setString('phpserver', 'https://upload.viiamanager.com/');
        ls.setString('uploads_base_url', 'https://uploads.viiamanager.com/');
    }
}
exports.deleteConfig = function() {
    if (ls.getBool('isRecord')) {
        const user = ls.getString('user');
        const pass = ls.getString('pass');
        const l = ls.getString('l');
        ls.clear();
        ls.setBool('isRecord', true);
        ls.setString('user', user);
        ls.setString('pass', pass);
        ls.setString('l', l);
    } else {
        ls.clear();
    }
}