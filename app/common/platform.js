const Device = require('@nativescript/core');
exports.getPlatform = function() {
    console.log('getPlatform()');
    if (Device.isAndroid) {
        return 'android';
    }
    if (Device.isAndroid) {
        return 'ios';
    }
    return 'android';
}
exports.getScreenDimensions = function(tipo) {
    switch (tipo) {
        case 'height':
            return (Device.Screen.mainScreen.heightDIPs);
        default:
            return (Device.Screen.mainScreen.widthDIPs);
    }
}
exports.getSdkVersion = function() {
    return parseInt(Device.Device.sdkVersion);
}