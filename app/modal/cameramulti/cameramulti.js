const ViewModel = require('./cameramulti-view-model');

let closeCallback;
let page;
//CAMERA
let cameraView = false;
let isInit = true;
const CameraPlus = require('@nstudio/nativescript-camera-plus').CameraPlus;
//
exports.onShownModally = function(args) {
    page = args.object;
    if (args.context.platform == 'android') {
        // eslint-disable-next-line no-undef
        page._dialogFragment.getDialog().getWindow().setBackgroundDrawable(new android.graphics.drawable.ColorDrawable(android.graphics.Color.TRANSPARENT));
    }
    const item = args.context.params;
    closeCallback = args.closeCallback;
    setDefaultParameters();
    for (const i in item) {
        ViewModel.set(i, item[i]);
    }
    page.bindingContext = ViewModel;
    setFolderPath();
    setTimeout(function() {
        cameraView = page.getViewById("cameraView");
        if (isInit) {
            setEventListenerCamera();
        }
        ViewModel.set('loaded', true);
        setFlash();
    }, 1000);
}

function setDefaultParameters() {
    ViewModel.set('isArray', true);
    ViewModel.set('durationmin', 4);
    ViewModel.set('rotate', false);
}

function setFolderPath() {
    let path = ViewModel.get('folderPath');
    path = path.replace('/data/data/', '/storage/emulated/0/Android/data/');
    ViewModel.set('folderPath', path);
}
exports.tapFlash = function() {
    if (ViewModel.get('loaded')) {
        cameraView.toggleFlash();
        setFlash();
    }
}

function setFlash() {
    if (cameraView.getFlashMode() == 'on') {
        ViewModel.set('flash', true);
    } else { //off
        ViewModel.set('flash', false);
    }
}
exports.itemTap_camera = function() {
    if (ViewModel.get('loaded')) {
        console.log('itemTap_camera()');
        closeCallback(ViewModel.get('value'), ViewModel.get('index'));
    }
}
exports.hideModal = function(args) {
    if (ViewModel.get('loaded')) {
        if (args.object.row == 1) {
            closeCallback(null, ViewModel.get('index'));
        } else {
            closeCallback(false);
        }
    }
}

exports.tapClear = function() {
    ViewModel.set('value', null);
}

function onEvent(args) {
    console.log('onEvent()');
    setContent(args);
}

function getFileName(url) {
    const last = url.lastIndexOf('/');
    url = url.substring(last + 1, url.length);
    return url;
}

function getCapturedAt(name) {
    name = name.replace('VID_', '').replace('.mp4', '');
    name = name.replace('PIC_', '').replace('.jpg', '');

    const y = name.substring(0, 4);
    const m = name.substring(4, 6);
    const d = name.substring(6, 8);
    const hour = name.substring(8, 10);
    const min = name.substring(10, 12);
    const seg = name.substring(12, 14);
    return y + '-' + m + '-' + d + ' ' + hour + ':' + min + ':' + seg;

}

function setContent(file) {
    const name = getFileName(file);
    const captured_at = getCapturedAt(name);
    let value = ViewModel.get('value');
    const isArray = ViewModel.get('isArray');
    if (ViewModel.get('isImage')) {
        if (isArray) {
            if (value) {
                value.push({
                    src: file,
                    name: name,
                    type: 'image',
                    captured_at: captured_at,
                    rotate: ViewModel.get('rotate')
                });
            } else {
                value = [{
                    src: file,
                    name: name,
                    type: 'image',
                    captured_at: captured_at,
                    rotate: ViewModel.get('rotate')
                }];
            }
        } else {
            value = [{
                src: file,
                name: name,
                type: 'image',
                captured_at: captured_at,
                rotate: ViewModel.get('rotate')
            }];
        }
    } else {
        if (isArray) {
            if (value) {
                value.push({
                    src: file,
                    name: name,
                    type: 'video',
                    captured_at: captured_at,
                    duration: ViewModel.get('duration'),
                    rotate: ViewModel.get('rotate')
                });
            } else {
                value = [{
                    src: file,
                    name: name,
                    type: 'video',
                    captured_at: captured_at,
                    duration: ViewModel.get('duration'),
                    rotate: ViewModel.get('rotate')
                }];
            }
        } else {
            value = [{
                src: file,
                name: name,
                type: 'video',
                captured_at: captured_at,
                duration: ViewModel.get('duration'),
                rotate: ViewModel.get('rotate')
            }];
        }
        if (ViewModel.get('recording')) {
            recordVideo(true);
        }
    }
    ViewModel.set('value', value);
    ViewModel.set('loaded', true);
    ViewModel.set('total', value.length);
}

function setEventListenerCamera() {
    console.log('NEW LISTENER');
    try {
        cameraView.on(CameraPlus.photoCapturedEvent, (event) => {
            console.log('photoCapturedEvent');
            onEvent(event.data.android);
        });
        cameraView.on(CameraPlus.videoRecordingStartedEvent, () => {
            console.log('videoRecordingStartedEvent');
        });
        cameraView.on(CameraPlus.videoRecordingReadyEvent, (event) => {
            console.log('videoRecordingReadyEvent');
            onEvent(event.data);
        });
    } catch (e) {
        console.log(e);
    }
}

function isResetValues() {
    if (ViewModel.get('isArray') == false) {
        if (ViewModel.get('total') > 0) {
            ViewModel.set('value', null)
            ViewModel.set('total', 0)
            return false;
        }
    }
    return true;
}
exports.takePhoto = function() {
    console.log('takePhoto()');
    if (ViewModel.get('loaded') && isResetValues()) {
        ViewModel.set('isImage', true);
        ViewModel.set('intents', ViewModel.get('intents') + 1);
        cameraView.requestCameraPermissions().then(() => {
            if (!cameraView) {
                cameraView = new CameraPlus();
            }
            const params = {
                confirm: false,
                saveToGallery: false,
                keepAspectRatio: true,
                autoSquareCrop: false,
                folderPath: ViewModel.get('folderPath')
            };
            if (ViewModel.get('rotate')) {
                params.width = 640;
                params.height = 480;
            } else {
                params.width = 480;
                params.height = 640;
            }
            cameraView.takePicture(params)
        });
    }
}

function stopRecording() {
    if (ViewModel.get('duration') >= ViewModel.get('durationmin')) {
        console.log('stopRecording()');
        ViewModel.set('recording', false);
        try {
            cameraView.stop();
            clearTimeout(timer);
        } catch (e) {
            console.log(e);
        }
    }
}

function startRecording() {
    console.log('startRecording()');
    console.log('folderPath:', ViewModel.get('folderPath'))
    ViewModel.set('recording', true);
    ViewModel.set('duration', 0);
    try {
        clearTimeout(timer);
        cameraView.requestCameraPermissions().then(() => {
            if (!cameraView) {
                cameraView = new CameraPlus();
            }
            const params = {
                quality: 'HIGHEST', //'MAX_480P',
                confirm: false,
                saveToGallery: false,
                folderPath: ViewModel.get('folderPath')
            };
            if (ViewModel.get('rotate')) {
                params.width = 640;
                params.height = 480;
            } else {
                params.width = 480;
                params.height = 640;
            }
            cameraView.record(params);
            countDurationRecordVideo();
        });
    } catch (e) {
        console.log(e);
    }
}

function recordVideo(close) {
    if (ViewModel.get('recording') || close) {
        stopRecording();
    } else {
        startRecording();
    }
}
exports.recordVideo = function() {
    if (ViewModel.get('loaded') && isResetValues()) {
        recordVideo();
    }
}
let timer;

function countDurationRecordVideo() {
    if (timer) {
        clearTimeout(timer);
    }
    timer = setInterval(() => {
        ViewModel.set('duration', ViewModel.get('duration') + 1);
    }, 1000);
}
//